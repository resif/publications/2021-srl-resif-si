# Source files for the 2021 Résif-SI article on SRL

These are the LaTeX source files for:

> Péquegnat et al., 2021, Résif-SI: a distributed information system for French
> seismological data. Seismological Research Letters 2021;
> doi: [10.1785/0220200392](https://doi.org/10.1785/0220200392)


## Compiling

To compile these files, make sure that you have a working [LaTeX installation],
with `pdflatex` and `bibtex`, then run `make`.

You should obtain the file `2021-SRL-Resif-SI.pdf`.


[LaTeX installation]: https://www.latex-project.org/get/
